import java.util.Iterator;
import java.util.NoSuchElementException;

public class UnidirectionalList<T> implements Iterable<T> {
    private ListNode<T> start;
    private ListNode<T> end;

    private int length;

    public UnidirectionalList() {}

    public UnidirectionalList(T ...items) {
        if (items == null) {
            throw new IllegalArgumentException("Items is null.");
        }

        for(var item : items) {
            addCore(item);
        }
    }

    public void add(T value) {
        this.add(value, length);
    }

    public void add(T value, int index) {
        if (index < 0 || index > length) {
            throw new IllegalArgumentException("Index must be positive and less than and equal to length");
        }

        addCore(value, index);
    }

    public void replace(T value, int index) {
        validateIndex(index);

        var targetNode = getElementByIndex(index);

        targetNode.value = value;
    }

    public void delete(int index) {
        validateIndex(index);

        deleteCore(index);

        length--;
    }

    public T get(int index) {
        validateIndex(index);

        return getElementByIndex(index).value;
    }

    public int getLength() {
        return length;
    }

    @Override
    public Iterator<T> iterator() {
        return new UnidirectionalListIterator(start);
    }

    private ListNode<T> getElementByIndex(int index) {
        if (index == 0) {
            return start;
        }

        if (index == length - 1) {
            return end;
        }

        var currentNode = start;

        while(index-- > 0) {
            currentNode = currentNode.next;
        }

        return currentNode;
    }

    private void validateIndex(int index) {
        if (index < 0 || index >= length) {
            throw new IllegalArgumentException("Index must be positive and less than length");
        }
    }

    private void deleteCore(int index) {
        if (index == 0) {
            if (start.next == null) {
                end = null;
            }

            start = start.next;
            return;
        }

        var targetNode = getElementByIndex(index - 1);

        targetNode.next = targetNode.next.next;
        if (targetNode.next == null) {
            end = targetNode;
        }
    }

    private void addCore(T value) {
        addCore(value, length);
    }

    private void addCore(T value, int index) {
        var newNode = new ListNode(value);

        insertNode(newNode, index);

        length++;
    }

    private void insertNode(ListNode node, int index) {
        // if list is empty
        if (start == null) {
            start = node;
            end = start;
            return;
        }

        // if we insert element at the beginning of the list
        if (index == 0) {
            node.next = start;
            start = node;
            return;
        }

        // if we insert element to the end of the list
        if (index == length) {
            end.next = node;
            end = node;
            return;
        }

        var insertionPositionNode = getElementByIndex(index - 1);

        node.next = insertionPositionNode.next;
        insertionPositionNode.next = node;
    }

    private class ListNode<T> {
        private T value;

        private ListNode next;

        public ListNode(T value) {
            this.value = value;
        }
    }

    private class UnidirectionalListIterator implements Iterator<T> {
        private ListNode<T> current;

        public UnidirectionalListIterator(ListNode<T> current) {
            this.current = current;
        }

        @Override
        public boolean hasNext() {
            return current != null;
        }

        @Override
        public T next() {
            if (current == null) {
                throw new NoSuchElementException();
            }

            var value = current.value;
            current = current.next;

            return value;
        }
    }
}
