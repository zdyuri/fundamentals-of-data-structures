import java.util.Arrays;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class CustomArrayList<T> implements Iterable<T>{

    private final int DEFAULT_ARRAY_SIZE = 5;
    private Object[] array;
    private int size = 0;

    public CustomArrayList() {
        array = new Object[DEFAULT_ARRAY_SIZE];
    }

    public CustomArrayList(T ...items) {
        if (items == null) {
            throw new IllegalArgumentException("Items is null.");
        }

        array = new Object[items.length];

        for(var item : items) {
            add(item);
        }
    }

    public T getElementByIndex(int index) {
        validateIndex(index);

        return (T) array[index];
    }

    private void extendArray() {
        array = Arrays.copyOf(array, size * 2);
    }

    public void add(T element) {
        if (size == array.length) {
            extendArray();
        }

        array[size] = element;
        size++;
    }

    public void add(T element, int index) {
        validateIndex(index);

        if (size == array.length) {
            extendArray();
        }

        System.arraycopy(array, index, array, index + 1,size - index);
        array[index] = element;
        size++;
    }

    public void replace(T element, int index) {
        validateIndex(index);

        array[index] = element;
    }

    public void delete(int index) {
        validateIndex(index);

        System.arraycopy(array, index + 1, array, index, size - index - 1);
        size--;
    }

    private void validateIndex(int index) {
        if (index >= array.length || index < 0) {
            throw new IndexOutOfBoundsException();
        }
    }

    @Override
    public Iterator<T> iterator() {
        return new CustomArrayListIterator();
    }

    private class CustomArrayListIterator implements Iterator<T> {
        private int index;

        @Override
        public boolean hasNext() {
            return index < size;
        }

        @Override
        public T next() {
            if (index >= size) {
                throw new NoSuchElementException();
            }

            return (T) array[index++];
        }
    }
}
